package graph.editor;

import graph.util.MyUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;

public class MyGraphCreator {

	
	int correctionShort = 0x0000FFFF; //Used to correct  addresses
	int correctionByte = 0x000000FF; //Used to correct  addresses
	JFrame frame = new JFrame();
	JPanel toolBar = new JPanel();
	
	/**
	 * Le contenu du dump
	 */
	String[] dumpSt;
	/**
	 * La taille du dump
	 */
	int dumpLength;
	
	/**
	 * Adresse de debut du dump
	 */
	int adressDumpBegin;
	
	/**
	 * Contient les reelles adresses des objets sur le dump
	 */
	ArrayList<Integer> objectAddresses = new ArrayList<Integer>(); 
	
	
	mxGraph graph;
	Object parent;
	mxGraphComponent graphComponent;
	
	Map<Integer, Object> nodes = new LinkedHashMap<Integer, Object>();
	
	Map<Integer, Object> nodesObject = new LinkedHashMap<Integer, Object>();
	
	Map<Integer, Object> visitedNodes = new LinkedHashMap<Integer, Object>();
	
	/*ArrayList of differet types, used to verifications*/
	ArrayList<Integer> objectType = new ArrayList<Integer>();  
	ArrayList<Integer> arrayType = new ArrayList<Integer>();  
	ArrayList<Integer> transientType = new ArrayList<Integer>();  
	ArrayList<Integer> allArrayType = new ArrayList<Integer>();  
	ArrayList<Integer> transientObject = new ArrayList<Integer>(); 
	/**/
	
	/**
	 * Map&lt;addressNode, Map&lt;ListPrev, ListNext&gt;&gt;
	 */
	Map<Integer, ArrayList<Integer>> mapPrev = new HashMap<Integer, ArrayList<Integer>>();
	Map<Integer, ArrayList<Integer>> mapNext = new HashMap<Integer, ArrayList<Integer>>();
	
	
	int xLevelSpace = 50;
	int yLevelspace = 200;
	int arraySpace = 150;
	int[][] levels = new int[100][1];
	
	
	public MyGraphCreator(mxGraphComponent component, String[] dataSt, int adressDumpBegin, ArrayList<Integer> objectAddresses, Map<Integer, ArrayList<Integer>> mapPrev, Map<Integer, ArrayList<Integer>> mapNext) {
		
		this.dumpSt = dataSt;
		this.adressDumpBegin = adressDumpBegin;
		this.objectAddresses = objectAddresses;
		this.mapPrev = mapPrev; 
		this.mapNext = mapNext; 
		
		//Initialiser les differents arraylist des objets.
		initializeLists();
		
		graphComponent = component;
		graph = graphComponent.getGraph();		
		parent = graph.getDefaultParent();
	}
	
	
	public void draw(){

		int yLevel = 0;
		graph.getModel().beginUpdate();

		try
		{
			int i = 0;
			for (Integer adresse : objectAddresses) {

				Object v1 = null;
				//Remplir la liste des objets
				int type = MyUtils.StbyteHexToByte(dumpSt[adresse-adressDumpBegin-6]) & correctionByte;
				yLevel = mapPrev.get(adresse).size();
				if (!objectType.contains(type)) {
					//x,y,width,heigh
					if (transientType.contains(type)) {
						v1 = graph.insertVertex(parent, String.format("@0x%04x", adresse), String.format("@0x%04x", adresse), (levels[yLevel][0] * xLevelSpace/2), (yLevel*yLevelspace) + arraySpace, 80, 30, "fillColor=blue");
						levels[yLevel][0] = levels[yLevel][0] + 1;
					}
					else if (transientObject.contains(type)) {
						v1 = graph.insertVertex(parent, String.format("@0x%04x", adresse), String.format("@0x%04x", adresse), levels[yLevel][0] * xLevelSpace, (yLevel*yLevelspace) + arraySpace, 80, 30, "fillColor=red");
						levels[yLevel][0] = levels[yLevel][0] + 1;
					}
					else{
						v1 = graph.insertVertex(parent, String.format("@0x%04x", adresse), String.format("@0x%04x", adresse), levels[yLevel][0] * xLevelSpace, (yLevel*yLevelspace) + arraySpace, 80, 30, "fillColor=green");
						levels[yLevel][0] = levels[yLevel][0] + 1;
					}

					nodes.put(adresse, v1);
				}
				else{
					System.out.println(String.format("@0x%04x", adresse) + "  : ylevel : " + yLevel + " xLevel : " + levels[yLevel][0]);

					v1 = graph.insertVertex(parent, String.format("@0x%04x", adresse), String.format("@0x%04x", adresse), levels[yLevel][0] * xLevelSpace, yLevel*yLevelspace, 80, 30, "fillColor=white");
					nodes.put(adresse, v1);
					nodesObject.put(adresse, v1);
					levels[yLevel][0] = levels[yLevel][0] + 1;
				}


			}
		}
		finally
		{
			graph.getModel().endUpdate();
		}
	}
	
	public void drawCustom(){

		int yLevel = 0;
		graph.getModel().beginUpdate();
		levels =  new int[100][1];
		try
		{
			int i = 0;
			mxCell myCell;
			for (Integer adresse : objectAddresses) {

				Object v1 = null;
				//Remplir la liste des objets
				int type = MyUtils.StbyteHexToByte(dumpSt[adresse-adressDumpBegin-6]) & correctionByte;
				myCell = (mxCell) ((mxGraphModel)graph.getModel()).getCell(String.format("@0x%04x", adresse));
				yLevel = mapPrev.get(adresse).size();
				
				if (!objectType.contains(type)) {
					//x,y,width,heigh
					myCell.setGeometry(new mxGeometry((levels[yLevel][0] * xLevelSpace/2), (yLevel*yLevelspace), myCell.getGeometry().getWidth(), myCell.getGeometry().getHeight()));
					levels[yLevel][0] = levels[yLevel][0] + 1;
				}
				else{
//					System.out.println(String.format("@0x%04x", adresse) + "  : ylevel : " + yLevel + " xLevel : " + levels[yLevel][0]);
					myCell.setGeometry(new mxGeometry(levels[yLevel][0] * xLevelSpace, yLevel*yLevelspace, myCell.getGeometry().getWidth(), myCell.getGeometry().getHeight()));
					levels[yLevel][0] = levels[yLevel][0] + 1;
				}
			}
		}
		finally
		{
			graph.refresh();
			graph.getModel().endUpdate();
		}
	}
	
	public void draw2(){
		graph.getModel().beginUpdate();
		try
		{

			for (Entry<Integer, Object> node : nodes.entrySet()) {

				int i = 0;
				int lengthNext = mapNext.get(node.getKey()).size();
				System.out.println(String.format("@0x%04x", (node.getKey())));
				System.out.println(lengthNext);

				double centerX = (((mxCell)node.getValue()).getGeometry().getX() + ((mxCell)node.getValue()).getGeometry().getWidth()/2) ;
				double centerY = (((mxCell)node.getValue()).getGeometry().getY() + ((mxCell)node.getValue()).getGeometry().getHeight()/2) ;

				for (Integer subNext : mapNext.get(node.getKey())) {

					System.out.println(String.format("@0x%04x", subNext));
					//Remplir la liste des objets
					int type = MyUtils.StbyteHexToByte(dumpSt[subNext-adressDumpBegin-6]) & correctionByte;
					//					
					if (!objectType.contains(type)) {
						System.out.println(((mxCell)nodes.get(subNext)).getGeometry().toString());


						//rotate a vertex around a certain point

						int point2X = (int) (((mxCell)node.getValue()).getGeometry().getX() + 100);
						int point2Y = (int) (((mxCell)node.getValue()).getGeometry().getY() + 100);
						double x = centerX + ( Math.cos(i*20) * (point2X-centerX) + Math.sin(i*20) * (point2Y -centerY));
						double y= centerY + ( -Math.sin(i*20) * (point2X-centerX) + Math.cos(i*20) * (point2Y -centerY));

						((mxCell)nodes.get(subNext)).getGeometry().setY(y);
						((mxCell)nodes.get(subNext)).getGeometry().setX(x);


					}
					else{

						//						int point2X = (int) (((mxCell)node.getValue()).getGeometry().getX() + 300);
						//			            int point2Y = (int) (((mxCell)node.getValue()).getGeometry().getY() + 300);
						//			            double x = centerX + ( Math.cos(i*20) * (point2X-centerX) + Math.sin(i*20) * (point2Y -centerY));
						//			            double y= centerY + ( -Math.sin(i*20) * (point2X-centerX) + Math.cos(i*20) * (point2Y -centerY));
						//			            
						//			            ((mxCell)nodes.get(subNext)).getGeometry().setY(y);
						//						((mxCell)nodes.get(subNext)).getGeometry().setX(x);
					}

					i++;
				}

			}

		}
		finally
		{
			graph.refresh();
			graph.getModel().endUpdate();
		}

	}
	
	
	/**
	 * Cette methode cree les differentes relation existante entre les objets :
	 * 
	 * Elle parcours les instances de classe a la recherhce des relations, des references avec les autres objets exisatnt dans mon enesemble d'objet
	 */
	public void createRelation(){
		
		for ( Entry<Integer, Object> nodeObj : nodesObject.entrySet()) {
			//Verifier pour chaque objet s'il contient des references 
			searchRelation(nodeObj.getKey());
		}
		
	}
	
	/**
	 * Cette methode prend une adresse d'une instance et cherche si elle contient des references 
	 * vers d'autres objets, si on trouve une adresse  vers un objet existant on cree un lien entre eux.
	 * 
	 * @param address
	 */
	private void searchRelation(int address){
		
		int indexDump = address - adressDumpBegin;
		
		int length = MyUtils.StshortHexToShort(dumpSt[indexDump-8]+dumpSt[indexDump-7]) & correctionShort;
		length = length - (length%2);//pour elliminer les longeurs impaires

		
		for (int i = 0; i < length; i+=2) {
			
			int candidate = MyUtils.StshortHexToShort( dumpSt[indexDump+i]+dumpSt[indexDump+i+1]) & correctionShort;
			if (objectAddresses.contains(candidate)) {
				
				System.out.println("++++candidate : " + String.format("%x", candidate));
				addEges(nodes.get(address), nodes.get(candidate));
				updatePosition(nodes.get(address));
				
			}
		}
		System.out.println("########################");
	}
	
	/**
	 * Cette methode prend deux noeuds et cree un edge entre eux.
	 * @param v1
	 * @param v2
	 */
	private void addEges(Object v1, Object v2){
		
		graph.insertEdge(parent, null, "..", v1, v2, mxConstants.STYLE_MOVABLE + "=0" );
		
//		((mxCell)v1).insert((mxCell)v2);
		
	}
	
	public void updatePosition(Object object){
		System.out.println("*******Edges de l'adresse : " + ((mxCell)object).getValue() +" " +((mxCell)object).getChildCount());		
	}
	
	
	public void selectChildeLevel(Object cell){
		
		int level = Integer.valueOf(JOptionPane.showInputDialog("Selected Level Hierachies"));
		int address = MyUtils.StshortHexToShort(graph.getLabel(cell).substring(3)) & correctionShort;
		graph.setSelectionCells(selectedLevel(address, level, new ArrayList<Object>(), ""));
		visitedNodes.clear();

	}
	
	private Object[] selectedLevel(int addressParent, int level, ArrayList<Object> selected, String indent){
		
		if (level > -1) {
			
			if (!visitedNodes.containsKey(addressParent)) {
				System.out.println(indent+ ((mxCell)nodes.get(addressParent)).getValue()+"("+((mxCell)nodes.get(addressParent)).getClass().getName()+")");
				selected.add(((mxCell)nodes.get(addressParent)));
				visitedNodes.put(addressParent, ((mxCell)nodes.get(addressParent)));
				int nbChilds = mapNext.get(addressParent).size();
				indent = indent + "  ";
				System.out.println("Childsss = " + nbChilds);
				for (int i=0; i< nbChilds ; i++) {
					System.out.println("Call child = " + i + " Level : "+ level +" ==> " + String.format("@0x%04x", mapNext.get(addressParent).get(i))) ;
					selectedLevel(mapNext.get(addressParent).get(i), (level - 1), selected, indent);
				}
			}
			
			
		}
		return selected.toArray();

	}
	
	
	public void displayHierachical(int addressParent, String indent){
		
		if (!visitedNodes.containsKey(addressParent)) {
			System.out.println(indent+ ((mxCell)nodes.get(addressParent)).getValue()+"("+((mxCell)nodes.get(addressParent)).getClass().getName()+")");
			visitedNodes.put(addressParent, ((mxCell)nodes.get(addressParent)));
			int nbChilds = mapNext.get(addressParent).size();
			indent = indent + "  ";
			for (int i=0; i<nbChilds ; i++) {
				displayHierachical(mapNext.get(addressParent).get(i), indent);
			}
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private void initializeLists() {
		/*objectType*/
		objectType.add((int) 0x80);
		objectType.add((int) 0xC0);
		objectType.add((int) 0x88);
		objectType.add((int) 0xC8);
		objectType.add((int) 0x84);
		objectType.add((int) 0xC4);
		
		/*arrayType*/
		arrayType.add((int)0x81);
		arrayType.add((int)0xC1);
		arrayType.add((int)0x82);
		arrayType.add((int)0xC2);
		arrayType.add((int)0x83);
		arrayType.add((int)0xC3);
		arrayType.add((int)0x89);
		arrayType.add((int)0xC9);
		
		/*transienType*/
		transientType.add((int)0x91);
		transientType.add((int)0xD1);
		transientType.add((int)0x92);
		transientType.add((int)0xD2);
		transientType.add((int)0x93);
		transientType.add((int)0xD3);
		transientType.add((int)0x94);
		transientType.add((int)0xD4);
		
		transientType.add((int)0x99);
		transientType.add((int)0xD9);
		transientType.add((int)0x9A);
		transientType.add((int)0xDA);
		transientType.add((int)0x9B);
		transientType.add((int)0xDB);
		transientType.add((int)0x9C);
		transientType.add((int)0xDC);
		
		/*Transient Objetc*/
		transientObject.add((int)0x94);
		transientObject.add((int)0xD4);
		transientObject.add((int)0x9C);
		transientObject.add((int)0xDC);
		
		/**/
		allArrayType.addAll(arrayType);
		allArrayType.addAll(transientType);
		
	}

}
