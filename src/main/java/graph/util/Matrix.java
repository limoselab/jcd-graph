package graph.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


/**
 * @author Abdou
 *
 */
public class Matrix {
	
	int correctionShort = 0x0000FFFF; //Used to correct  addresses
	int correctionByte = 0x000000FF; //Used to correct  addresses
	/**
	 * Le contenu du dump
	 */
	String[] dumpSt;
		
	/**
	 * Adresse de debut du dump
	 */
	int addressDumpBegin;
	
	/**
	 * Contient les reelles adresses des objets sur le dump
	 */
	ArrayList<Integer> objectAddresses = new ArrayList<Integer>(); 
	
	/*ArrayList of differet types, used to verifications*/
	ArrayList<Integer> objectType = new ArrayList<Integer>();  
	ArrayList<Integer> arrayType = new ArrayList<Integer>();  
	ArrayList<Integer> transientType = new ArrayList<Integer>();  
	ArrayList<Integer> allArrayType = new ArrayList<Integer>();  
	ArrayList<Integer> transientObject = new ArrayList<Integer>(); 
	/**/
	
	/**
	 * Map&lt;addressNode, Map&lt;ListPrev, ListNext&gt;&gt;
	 */
	public Map<Integer, ArrayList<Integer>> mapPrev = new HashMap<Integer, ArrayList<Integer>>();
	public Map<Integer, ArrayList<Integer>> mapNext = new HashMap<Integer, ArrayList<Integer>>();

	public Matrix(String[] dataSt, int addressDumpBegin, ArrayList<Integer> objectAddresses) {
		this.dumpSt = dataSt;
		this.addressDumpBegin = addressDumpBegin;
		this.objectAddresses = objectAddresses;
		initializeLists();
		initializeMap();
	}
	
	
	
	private void initializeMap() {
		
		for (int i = 0; i < objectAddresses.size(); i++) {
			mapPrev.put(objectAddresses.get(i), new ArrayList<Integer>());
			mapNext.put(objectAddresses.get(i), new ArrayList<Integer>());
		}		
		
	}



	/**
	 * Cette methode prend une adresse d'une instance et cherche si elle contient des references 
	 * vers d'autres objets, si on trouve une adresse  vers un objet existant on cree un lien entre eux.
	 * 
	 * @param address
	 */
	public void searchRelation(){
		System.out.println("###################SearchRelation#######################");
		
		for (int i = 0; i < objectAddresses.size(); i++) {
			int indexDump = objectAddresses.get(i) - addressDumpBegin;
//			System.out.println("indexDump = " + indexDump);
			int length = MyUtils.StshortHexToShort(dumpSt[indexDump-8]+dumpSt[indexDump-7]) & correctionShort;
			length = length - (length%2);//pour elliminer les longeurs impaires
			int type = MyUtils.StbyteHexToByte(dumpSt[indexDump-6]) & correctionByte;
//			System.out.println(length);
//			System.out.println(type);
			
			System.out.println("+Current Address : " + String.format("%X", objectAddresses.get(i)));
			
			for (int j = 0; j < length; j+=2) {
				if ( dumpSt.length <= indexDump+j+1 ) {
					System.out.println("-------------------------------------------------------------------");
					System.out.println("######################## End File");
					return;
				}
				int candidate = MyUtils.StshortHexToShort( dumpSt[indexDump+j]+dumpSt[indexDump+j+1]) & correctionShort;
				System.out.println("+candidate : " + String.format("%X", candidate));
				if (objectAddresses.contains(candidate)) {
					// x1-->x3 ; x1-->x4
					//x1.ListNext = {x3, x4} ; x3.ListPrev = {...,x1}; x4.ListPrev = {..., x1}
					if (!allArrayType.contains(type)) {
						if (!mapNext.get(objectAddresses.get(i)).contains(candidate)) {
							//The candidate address is added to the list of the next addresses
							mapNext.get(objectAddresses.get(i)).add(candidate);
						}
					}
					
					if (!mapPrev.get(candidate).contains(objectAddresses.get(i))) {
						//The candidate address has as a previous address the current address
						mapPrev.get(candidate).add(objectAddresses.get(i));
					}
					
//					System.out.println("++++candidate : " + String.format("%x", candidate));
				}
			}
			System.out.println("-------------------------------------------------------------------");
//			System.out.println("++++ListNext : " + mapNext.get(objectAddresses.get(i)).size());
//			System.out.println("++++ListPrev : " + mapPrev.get(new Integer(0x42c & correctionShort)).size());
//			System.exit(0);
		}
		System.out.println("########################");
	}
	
	
	public void print(){
		System.out.println("Node ---- ListPrev ---- ListNext");
		for (int i = 0; i < objectAddresses.size(); i++) {
			System.out.println(String.format("%04x", objectAddresses.get(i)) 
					+ " ---- " + mapPrev.get(objectAddresses.get(i)).size() 
//					+ "==>" + printHexList(mapPrev.get(objectAddresses.get(i)))
					+ " ---- " + mapNext.get(objectAddresses.get(i)).size() 
//					+ "==>" + printHexList(mapNext.get(objectAddresses.get(i)))
			);
		}
		
	}
	
	public String printHexList(ArrayList<Integer> list){
		
		StringBuilder stb = new StringBuilder();
		stb.append("{");
		for (int i = 0; i < list.size(); i++) {
			stb.append(String.format("0x%04x", list.get(i)) + ", " );
		}
		stb.append("}");
		return stb.toString();
	}
	
	
	private void initializeLists() {
		/*objectType*/
		objectType.add((int) 0x80);
		objectType.add((int) 0xC0);
		objectType.add((int) 0x88);
		objectType.add((int) 0xC8);
		objectType.add((int) 0x84);
		objectType.add((int) 0xC4);
		
		/*arrayType*/
		arrayType.add((int)0x81);
		arrayType.add((int)0xC1);
		arrayType.add((int)0x82);
		arrayType.add((int)0xC2);
		arrayType.add((int)0x83);
		arrayType.add((int)0xC3);
		arrayType.add((int)0x89);
		arrayType.add((int)0xC9);
		
		/*transienType*/
		transientType.add((int)0x91);
		transientType.add((int)0xD1);
		transientType.add((int)0x92);
		transientType.add((int)0xD2);
		transientType.add((int)0x93);
		transientType.add((int)0xD3);
		transientType.add((int)0x94);
		transientType.add((int)0xD4);
		
		transientType.add((int)0x99);
		transientType.add((int)0xD9);
		transientType.add((int)0x9A);
		transientType.add((int)0xDA);
		transientType.add((int)0x9B);
		transientType.add((int)0xDB);
		transientType.add((int)0x9C);
		transientType.add((int)0xDC);
		
		/*Transient Objetc*/
		transientObject.add((int)0x94);
		transientObject.add((int)0xD4);
		transientObject.add((int)0x9C);
		transientObject.add((int)0xDC);
		
		
		/**/
		allArrayType.addAll(arrayType);
		allArrayType.addAll(transientType);
		
	}
	
	
	
	



}
