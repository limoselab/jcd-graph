package graph.util;

import java.util.ArrayList;

public class ParseDump {

	
	int correctionShort = 0x0000FFFF; //Used to correct  addresses
	int correctionByte = 0x000000FF; //Used to correct  addresses
	
	/**
	 * Le contenu du dump
	 */
	String[] dumpSt;
	/**
	 * La taille du dump
	 */
	int dumpLength;
	/**
	 * Adresse de debut du dump
	 */
	int adressDumpBegin;
	/**
	 * Adresse de fin du dump
	 */
	int addressDumpEnd;
	/**
	 * Adresse ancienne du dump, je l'utilise quand je restarte pour reinitialiser le current adresse
	 */
	public int addressDumpOld;
	/**
	 * l'adresse courrante
	 */
	public int currentDumpAddress;
	/**
	 * L'index dans le tableau des donnees.
	 */
	int indexDump = 0;
	/**
	 * L'index dans le tableau des donnees, je l'utilise quand je restarte pour reinitialiser le indexDump
	 */
	public int indexDumpOld = 0;
	
	int depthObject = 0;
	
	
	public ArrayList<Integer> objectAddresses = new ArrayList<Integer>(); 
	
	
	/**/
	ArrayList<Integer> objectType = new ArrayList<Integer>();  
	ArrayList<Integer> arrayType = new ArrayList<Integer>();  
	ArrayList<Integer> transientType = new ArrayList<Integer>();  
	ArrayList<Integer> transientObject = new ArrayList<Integer>();  
	ArrayList<Integer> contexts = new ArrayList<Integer>();
	ArrayList<Integer> indexPackages = new ArrayList<Integer>();
	
	/*Cet index est utiliser pour */
	int adressObjectsBegin;
	
	public ParseDump(String[] dumpSt, int addressDumpBegin ) {
		this.dumpSt = dumpSt;
		this.dumpLength = this.dumpSt.length;
		this.adressObjectsBegin = addressDumpBegin;
		this.addressDumpOld = addressDumpBegin; 
		this.addressDumpEnd = addressDumpBegin + dumpLength;
		this.currentDumpAddress = addressDumpBegin;
		
		//initializes liste des types et des contextes
		initializeLists();
	}	
	
	
	public void searchObject(){
		
		System.out.println("-----------------------------------------searchObject-----------------------------------------");
		//On verifie premierement qu'on est sur une bonne adresse
		
		boolean flagOk = false;
		
		
		//On doit parser un seul objet (tableau ou instance) vide
		//
		while (currentDumpAddress + 4 <= addressDumpEnd) 
		{
			//Il faut penser a verifier que la taille est bien suffisant pour au moins un objet
			System.out.println("Adresse : @" + String.format("%04x", currentDumpAddress));
			
//			if (currentDumpAddress % 4 == 0) 
			if (padding(currentDumpAddress) == 0) 
			{
				//On est sur la memoire 2
				int length = MyUtils.StshortHexToShort(dumpSt[indexDump]+dumpSt[indexDump+1]) & correctionShort;
				int type = MyUtils.StbyteHexToByte(dumpSt[indexDump+2]) & correctionByte;
				int context = MyUtils.StbyteHexToByte(dumpSt[indexDump+3]) & correctionByte;
//				System.out.println("ma taille : " + String.format("%04x", length));
				
				System.out.println("--Je suis une bonne adresse, on va verifier, si je suis un bon objet");
				System.out.println("==================== length = " + String.format("%x", length));
				System.out.println("==================== type = " + String.format("%x", type));
				System.out.println("==================== context = " + String.format("%x", context));
				
				//On augmente le currentDumpAddress avec 4 car on a parcouru le header\
				currentDumpAddress+=4;
				indexDump+=4;

				if (
						length <= 0x7FFF //une bonne taille
						&&
						(arrayType.contains(type) || objectType.contains(type) || transientType.contains(type)) //Un bon type
						&&
						contexts.contains(context)
						)	
				{
					/*
					 * Si j'ai a priori un bon objet
					 */
					
					System.out.println("----Je suis un bon objet mais je suis de quelle type ?");
					
					
					//C'est un tableau persistant
					if (arrayType.contains(type)) {
						
						/*
						 * Verifier si l objet trouver ne depasse pas de taille le dump
						 */
						if (currentDumpAddress + (length-1) <= addressDumpEnd) {

							System.out.println("--------Tableau Persistant : Je suis un bon tableau persistant on va voir mon next");
							
							//
							//Add the object's address to the arraylist of correct object
							objectAddresses.add(currentDumpAddress+4);
							
							currentDumpAddress = currentDumpAddress + length;
							indexDump = indexDump + length;
							depthObject++;
							

						}
						else{
							//TODO : il faut gerer le cas ou la taille du tableau trouver depasse le dump
							//la taille de mon tableau depasse le longueur du dump
							/*
							 * Soit le dump est incomplet
							 */
							/*
							 * Soit je suis tromper de chemin, je n ai pas pris le bon premier objet
							 */
							
							/*
							 * C'est indecidable
							 */
							
							System.out.println("!!!!!! "+String.format("%04x", currentDumpAddress) + " Ko technique, je depasse la taille du dump, je termine a : " + String.format("%x", currentDumpAddress + length) + " la taille du dump : " + String.format("%x", addressDumpEnd ));
							System.out.println(String.format("%04x", currentDumpAddress + length) + ">" + String.format("%04x", addressDumpEnd));
							
							/*
							 * 
							 * */
							if (currentDumpAddress == addressDumpOld+4) {
								/*
								 * Si mon premier objet trouver depasse la taille du dump
								 */
								System.out.println("#########################################");
								System.out.println("#                   2                   #");
								System.out.println("**j'ai pris l hypothese que le premier objet ne dois pas depasser la longueur de tout le dump");
								restart();
							}
							else{
								
								//Si je ne suis pas le premier objet est que je depasse la taille dump
								//Je vais verifier combien y a d'dobjet avant moi
								//Si ca satisfait u certain nombre donc je considere que 
								//j ai pris le bon chemin et que c'est le dump qui est incomplet.
								
								if (depthObject < 3) {
									restart();
								}
								else {
									System.out.println("C'est finis mon amis");
									return;
								}
							}
							
//							System.exit(0);
						}

					}

					//C'est un tableau transient
					if (transientType.contains(type)) {
						System.out.println("--------Tableau Transient : Je suis un bon tableau transient on va voir mon next");
						
						
						//Add the object's address to the arraylist of correct object
						objectAddresses.add(currentDumpAddress+4);
						
						//Je dois verifier s'il est de type object ou tableau
						if (transientObject.contains(type)) {
							
							//Les objet transient contient 12 octet d header
							currentDumpAddress = currentDumpAddress + 4;
							indexDump = indexDump + 4;
						}
						
						
						currentDumpAddress = currentDumpAddress + 4;
						indexDump = indexDump + 4;
						depthObject++;
					}

					//C'est un object
					if (objectType.contains(type)) {
						System.out.println("--------Instance : On ne s'est pas encore est ce que Je suis une bonne instance on va voir mon next");
						
						
						int indexpackage = MyUtils.StbyteHexToByte(dumpSt[indexDump+1]) & correctionByte;
						
						/*
						 * Verifier en plus l'indexpackage de l'instance
						 */
						if (!indexPackages.contains(indexpackage)) {
							
							/*
							 * Si mon premier objet trouver deasse la taille du dump
							 */
							System.out.println("#########################################");
							System.out.println("#                   3                   #");
							System.out.println("**Je ne suis pas une bonne instance, je n ai pas un bon indexpackage : " + indexpackage );
							restart();
							
						}
						
						
						/*
						 * Verifier si l objet trouver ne depasse pas de taille le dump
						 */
						if (currentDumpAddress + 4 + (length-1) <= addressDumpEnd) {

							//
							
							//Add the object's address to the arraylist of correct object
							objectAddresses.add(currentDumpAddress+4);
							
							currentDumpAddress = currentDumpAddress + 4 + length;
							indexDump = indexDump + 4 + length;
							depthObject++;

						}
						else{
							//TODO : il faut gerer le cas ou la taille du tableau trouver depasse le dump
							//la taille de mon tableau depasse le longueur du dump
							/*
							 * Soit le dump est incomplet
							 */
							/*
							 * Soit je suis tromper de chemin, je n ai pas pris le bon premier objet
							 */
							
							/*
							 * C'est indecidable
							 */
							
							System.out.println("!!!!!! "+String.format("%04x", currentDumpAddress) + " Ko technique, je depasse la taille du dump, je termine a : " + String.format("%x", currentDumpAddress + length) + " la taille du dump : " + String.format("%x", addressDumpEnd ));
							System.out.println(String.format("%04x", currentDumpAddress + length) + ">" + String.format("%04x", addressDumpEnd));
							
							/*
							 * 
							 * */
							if (currentDumpAddress == addressDumpOld+4) {
								/*
								 * Si mon premier objet trouver deasse la taille du dump
								 */
								System.out.println("#########################################");
								System.out.println("#                   4                   #");
								System.out.println("**j'ai pris l hypothese que le premier objet ne dois pas depasser la longueur de tout le dump");
								restart();
							}
							else{
								
								//Si je ne suis pas le premier objet est que je depasse la taille dump
								//Je vais verifier combien y a d'dobjet avant moi
								//Si ca satisfait u certain nombre donc je considere que 
								//j ai pris le bon chemin et que c'est le dump qui est incomplet.
								
								if (depthObject < 3) {
									restart();
								}
								else {
									System.out.println("C'est finis mon my freind");
									return;
								}
							}
							
						}
						
					}	
				}
				
				else{
					
					
					System.out.println("---- BEin non je ne suis pas un bon objet");
					
					if (depthObject < 3) {
						//
						restart();
					}
					else {
						//Je peux etre sur les bytes de la taille restante donc tout ce qui va suivre 
						//se sont des 00, alors je verifie, ca, si tout ce qui va suivre sont dez zero, alors
						//je suis sur le bon chemin, si non, je restart
						
						
						if (!verifyRestZero()) {
							restart();
						}
						else{
							System.out.println("Il ne reste plus que des zero jusqu a la fin dump, donc on a plus"
									+ "d'objet.");
							return;
						}
						
					}
					
					
					
				}
				
			}
			
			
			/*
			 * Si je ne suis pas sur une bonne adresse alors je ne me casserai pas la tete 
			 * a incrementer par un, mais je calcule pour voir comment je tombe sur une bonne addresse
			 * 
			 * Donc je dois trouver le padding a jouter : pour le trouver il faut savoir que le padding max soit 3 octets, 
			 * alors je fais le currentDumpAddress%4 cela va me donner ce qui reste donc la fin de l'adresse, alors je fais soustraire ca au 4 alors je trouverai le apdding
			 */
			int padding = padding(currentDumpAddress);
			currentDumpAddress = currentDumpAddress + padding;
			indexDump = indexDump + padding;

		}
		
		
	}

	
	/**
	 * Return true si tout ce qui reste dans le dump sont des zero ou false si non
	 * @return
	 */
	private boolean verifyRestZero(){
		int i = 0;
		while (indexDump+i < dumpLength) {
			
			if (!dumpSt[indexDump+i].equalsIgnoreCase("00")) {
				
				return false;
			}
			i++;
		}
		return true;
		
	}
	
	
	/**
	 * Cette methode fait un restarte de la boucle dans le sens ou elle reinitialise le currentDumpAddress
	 * a l'adresse du addressDumpOld + 4, ou on fait la recherche d'un cran par rapport a ce qu on a deja 
	 * verifier. 
	 *
	 */
	private void restart(){
		
		System.out.println("#########################################");
		System.out.println("#                Restart                #");
		System.out.println("#########################################");
		
		addressDumpOld = addressDumpOld+4+padding(addressDumpOld);
		currentDumpAddress = addressDumpOld;
		indexDumpOld = indexDumpOld + 4;
		indexDump = indexDumpOld;
		depthObject = 0;
		objectAddresses.clear();
		
	}

	
	/**
	 * Cette methode prend une adresse et retounrne le padding necessaire pour avoir une bonne 
	 * adresse : le padding a ajouter pour avoir une adresse qui se termine par 0,4,8,c
	 * @param address
	 * @return
	 */
	private int padding(int address){		
		return (4 - (address % 4))%4;
	}
	
	
	/**
	 * Cette methode prend une adresse et elle retourne le padding neccessaire pour avoir
	 * une adresse qui se termine par 2.
	 * Si l'adresse fournis termine deja par 2 alors le padding sera egale a 0.
	 * @param address
	 * @return
	 */
	private int padding2(int address){	
		
		
		int endAddress  = address & 0x0000000F;
		int check = 10 - endAddress;
		int padding = 0;
		if (check>=0) {
			padding = check;
		}
		else{
			padding = 16 - (endAddress-10);
		}
		
		return padding;
	}
	
	
	private void initializeLists() {
		/*objectType*/
		objectType.add((int) 0x80);
		objectType.add((int) 0xC0);
		objectType.add((int) 0x88);
		objectType.add((int) 0xC8);
		objectType.add((int) 0x84);
		objectType.add((int) 0xC4);
		
		/*arrayType*/
		arrayType.add((int)0x81);
		arrayType.add((int)0xC1);
		arrayType.add((int)0x82);
		arrayType.add((int)0xC2);
		arrayType.add((int)0x83);
		arrayType.add((int)0xC3);
		arrayType.add((int)0x89);
		arrayType.add((int)0xC9);
		
		/*transienType*/
		transientType.add((int)0x91);
		transientType.add((int)0xD1);
		transientType.add((int)0x92);
		transientType.add((int)0xD2);
		transientType.add((int)0x93);
		transientType.add((int)0xD3);
		transientType.add((int)0x94);
		transientType.add((int)0xD4);
		
		transientType.add((int)0x99);
		transientType.add((int)0xD9);
		transientType.add((int)0x9A);
		transientType.add((int)0xDA);
		transientType.add((int)0x9B);
		transientType.add((int)0xDB);
		transientType.add((int)0x9C);
		transientType.add((int)0xDC);
		
		
		/*Transient Objetc*/
		transientObject.add((int)0x94);
		transientObject.add((int)0xD4);
		transientObject.add((int)0x9C);
		transientObject.add((int)0xDC);
		
		/*Contexts*/
		int context = 0x00;
		for (int i = 0; i < 0x30; i++) {
			contexts.add(context);
			context+=2;
		}
		
		/*IndexPackages*/
		int indexpackage = 0x01;
		for (int i = 0; i < 0x30; i++) {
			indexPackages.add(indexpackage);
			indexpackage+=2;
		}
	}
	
	
	
	
	
	
}
