package graph.util;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MyUtils {
	
	/// the logger
    private final static Logger logger = LoggerFactory.getLogger(MyUtils.class);
	
    /**
	 * Take a string path of file with a value as String hex separated by spaces and return an array of a those strings
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static String[] load_spaces(String path) throws IOException
	{
    	BufferedReader br = new BufferedReader(new FileReader(path));
    	String octet ;
    	String[] data = null;
    	octet = br.readLine();
    	data = octet.split(" ");
    	br.close();
    	return data;
	}

	/**
	 * Take a string path of file with a value as String hex separated by new lines and return an array of a those strings
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static String[] load_lines (String path) throws IOException{
    	
    	BufferedReader br = new BufferedReader(new FileReader(path));
    	String octet ;
    	int i = 0;
    	while ((octet = br.readLine()) != null) {
    		i++;
    	}
    	br.close();
    	
    	BufferedReader br1 = new BufferedReader(new FileReader(path));
    	String[] data = new String[i];
    	i = 0;
    	
    	while ((octet = br1.readLine()) != null) {
    		data[i] = octet;
    		i++;
    	}
    	br1.close();
    	return data;
    }
	
	/**
	 * 
	 * @param content
	 * @param nameFile
	 * @param escape
	 * @throws IOException
	 */
	public static void saveIt(byte[] content, String nameFile, String escape) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> saveIt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		logger.info("************************");
		StringBuilder sb = new StringBuilder();
		for (byte b : content)
		{
			sb.append(String.format("%02X"+escape, b));
		}
//		System.out.println("Content of the array " + sb );
		logger.info("Content Saved in : " +nameFile);
		Writer writer = new FileWriter(nameFile);
		writer.write(sb.toString());
		writer.close();
	}
	
	
	/**
	 * 
	 * @param content
	 * @param nameFile
	 * @param escape
	 * @throws IOException
	 */
	public static void saveDevideIt(byte[] content, String nameFile, int length) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> saveIt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		logger.info("************************");
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < content.length; i++) {
			if (i!=0 && i%length == 0) {
				sb.append("\n\n"+String.format("%02X ", content[i]));
			}
			else{
				sb.append(String.format("%02X ", content[i]));
			}
		}
//		System.out.println("Content of the array " + sb );
		logger.info("Content Saved in : " +nameFile);
		Writer writer = new FileWriter(nameFile);
		writer.write(sb.toString());
		writer.close();
	}
	
	/**
	 * 
	 * @param content
	 * @param nameFile
	 * @param escape
	 * @throws IOException
	 */
	public static void stSaveDevideIt(String[] content, String nameFile, int length) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> saveIt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		logger.info("************************");
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < content.length; i++) {
			if (i!=0 && i%length == 0) {
				sb.append("\n\n"+content[i]+" ");
			}
			else{
				sb.append(content[i]+" ");
			}
		}
//		System.out.println("Content of the array " + sb );
		logger.info("Content Saved in : " +nameFile);
		Writer writer = new FileWriter(nameFile);
		writer.write(sb.toString());
		writer.close();
	}
	
	
	/**
	 * 
	 * @param content
	 * @param nameFile
	 * @param escape
	 * @throws IOException
	 */
	public static void stSaveIt(String[] content, String nameFile, String escape) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> saveIt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		logger.info("************************");
		StringBuilder sb = new StringBuilder();
		for (String b : content)
		{
			sb.append(b+escape);
		}
//		System.out.println("Content of the array " + sb );
		logger.info("Content Saved in : " +nameFile);
		Writer writer = new FileWriter(nameFile);
		writer.write(sb.toString());
		writer.close();
	}
	
	
	/**
	 * 
	 * @param content
	 * @throws IOException
	 */
	public static void printIt(byte[] content, String escape) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> printItToSearch <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		logger.info("************************");
		StringBuilder sb = new StringBuilder();
		for (byte b : content)
		{
			sb.append(String.format("%02X"+escape, b));
		}
//		System.out.println("Content of the array " + sb );
		logger.info("Content of the array ");
		logger.info(sb.toString());
	}
	
	/**
	 * 
	 * @param content
	 * @throws IOException
	 */
	public static void stPrintIt(String[] content, String escape) throws IOException
	{
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> StPrintIt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		System.out.println("************************");
				
		StringBuilder sb = new StringBuilder();
		for (String b : content)
		{
			sb.append(b+escape);
		}
		System.out.println(sb.toString());
	}
	
	/**
	 * Convert an array of strings hex to an array of shorts<br />
	 * Prend les string deux a deux pour contruire un short
	 * @param data
	 * @return
	 */
	public static int[] ArrayStringHexToShort(String[] data){
		int[] dataShort = new int[data.length];
    	for (int i = 0; i < dataShort.length; i++) {
    		dataShort[i] = (int) (StshortHexToShort(data[i])&0x0000FFFF);
		}
		return dataShort;
	}
	
	
	/**
	 * Convert an array of strings hex to an array of bytes
	 * @param data
	 * @return
	 */
	public static byte[] ArrayStringHexToByte(String[] data){
		byte[] dataByte = new byte[data.length];
    	for (int i = 0; i < dataByte.length; i++) {
    		dataByte[i] = StbyteHexToByte(data[i]);
		}
		return dataByte;
	}
	
	
	/**
	 * take a string short hex and return it as a short <br />
	 * "AAAA" ==> (short) AAAA
	 * @param stHex
	 * @return
	 */
	public static short StshortHexToShort(String stHex){
		return (short)getShort(StbyteHexToByte(stHex.substring(0, 2)), StbyteHexToByte(stHex.substring(2, 4)));
	}
	
	
	/**
	 * take a string byte hex and return it as a byte <br />
	 * "AA" ==> (byte) AA
	 * @param stHex
	 * @return
	 */
	public static byte StbyteHexToByte(String stHex){
		return (byte)Integer.parseInt(stHex, 16);
	}
	
	/**
	 * 
	 * @param byte1
	 * @param byte2
	 * @return
	 */
	public static short getShort(byte byte1, byte byte2) {
	    short theShort = byte1;
	    theShort <<= 8;
	    theShort += ((short) byte2) & 0xff;
	    return theShort;
	}
	
	
	public static void printSt(String[] array){
		for (String val : array) {
			System.out.println(val + " ");
		}
	}
	
	public static void printByte(byte[] array){
		for (byte val : array) {
			System.out.println(String.format("%02x", val) + " ");
		}
	}
	
	
	public static void copyToClipBoard(byte[] d) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < d.length; i++)
		{
			sb.append(String.format("%02X ", d[i]));
		}
		StringSelection stringSelection = new StringSelection(sb.toString());
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
	}
	
	
	public static boolean st_in_array(String[] array, String string){

		for (int i = 0; i < array.length; i++)
		{
			if(array[i].equalsIgnoreCase(string))
			{
				return true;
			}
		}
		return false;

	}
	
	
	public static byte[] shortToArryByte(short value){
		
		byte[] tabByte = new byte[2];
		tabByte[0] = (byte)((value >> 8) & 0xff);
		tabByte[1] = (byte)(value & 0xff);
		
		return tabByte;
	}
	
	
}
